"""
web scraper for uniprot.
simply recreating my excel macro, but what promises to be faster because of not needing to wait for the page to load.

1. establish scraping mechanism
2. code in logic, e.g. for putative match, or no matches.
3.
"""
import urllib, bs4
p = 'P63267' # 'P04626' erbb2
url = 'https://www.uniprot.org/uniprot/' + p

response = urllib.request.urlopen(url)
page = response.read()
soup = bs4.BeautifulSoup(page)
div = soup.find("div", {"id": "ptm_processing"})
begin_search_phrase = "Keywords - PTM"
begin_cut = div.text.find(begin_search_phrase) + len(begin_search_phrase) + 1
end_cut = div.text.find("Proteomic databases")
ptm = div.text[begin_cut:end_cut]

